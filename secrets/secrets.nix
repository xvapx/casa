# ssh keys used by agenix
let

  # users

  u_marti-casa = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAK7PSl4xpvZgAD2HstjuuyH0+5hCMV74a/oW7jgrB+a xvapx@xvapx";
  u_marti = [ u_marti-casa ];

  users = u_marti;

  # machines

  m_marti-casa = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMgyjXdXMMBv9Bh/yMILMe+qfp244dihki7Af9l6/V+V root@marti";
  m_marti = [ m_marti-casa ];

  hosts = m_marti;

in
{
  # google drive
  "marti_gdrive_id.age".publicKeys = u_marti ++ m_marti;
  "marti_gdrive_key.age".publicKeys = u_marti ++ m_marti;
  # VPN
  "marti_getflix_auth.age".publicKeys = users ++ hosts;
  "marti_getflix_ca.age".publicKeys = users ++ hosts;
  # local network
  "marti_smb_credentials.age".publicKeys = users ++ hosts;
}
