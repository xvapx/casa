{ config, lib, ...}:
{
  users.groups = {
    smb_users = {
      gid = 1999;
      members = [ "invitado" "admin" "iguana" "xvapx" ];
    };
    transmission = {
      gid = lib.mkForce config.ids.uids.transmission;
      members = [ "iguana" "xvapx" ];
    };
    amule = {
      gid = lib.mkForce config.ids.gids.amule;
      members = [ "iguana" "xvapx" ];
    };
  };
}
