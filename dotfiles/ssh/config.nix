{ network
, pre ? ""
, post ? ""
, home ? "~"
, auto-add ? false
}:

pre + ''
  Host supermicro
    HostName ${network.supermicro.ip}
    Port 22
    IdentityFile ${home}/.ssh/id_ed25519
''
+ (if auto-add then ''
  AddKeysToAgent yes

'' else "")
  + post
