{ lib, pkgs, config, dotfiles, localization, ... }:

let

  user_defaults = { name, uid, home }: {

    activation = {
      # create /var/run/${uid} and /nix/var/nix/profiles/per-user/${name}
      directories = ''
        ${pkgs.coreutils}/bin/mkdir -m 0700 -p /run/user/${toString uid} /nix/var/nix/profiles/per-user/${name}
        ${pkgs.coreutils}/bin/chown ${name}:users /run/user/${toString uid} /nix/var/nix/profiles/per-user/${name}
      '';
    };

    general = {
      inherit uid home;
      isNormalUser = true;
      createHome = true;
      group = "users";
      extraGroups = [ "audio" "cdrom" "networkmanager" "scanner" "video" ];
    };

    home-manager = {
      home = {
        # IMPORTANT: check https://rycee.gitlab.io/home-manager/release-notes.html before changing stateVersion
        stateVersion = "22.05";
        # dotfiles
        file = {
          # avoid KDE errors
          ".cache/kde/.keep".text = "";
          # localization
          ".bash_locale".text = localization.user.bash.ca;
          ".config/plasma-localerc".text = localization.user.plasma.ca;
        };
        sessionVariables = {
          XDG_CACHE_HOME = "${home}/.cache";
          XDG_CONFIG_HOME = "${home}/.config/";
          XDG_DATA_HOME = "${home}/.local/share";
          XDG_STATE_HOME = "${home}/.local/state";
          XDG_RUNTIME_DIR = "/run/user/${toString uid}";          
        };
      };
    };

  };

in
lib.recursive_merge [
  (import ./users/invitado.nix { inherit user_defaults dotfiles; })
  (import ./users/xvapx.nix { inherit lib pkgs user_defaults dotfiles localization; })
  (import ./users/iguana.nix { inherit user_defaults dotfiles; })
]
