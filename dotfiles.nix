{ pkgs, lib, network }:

{

  ssh.config = args@{...}: import ./dotfiles/ssh/config.nix ({ network = network.casa.services; } // args);

}
