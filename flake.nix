{
  description = "Nix configuration for Marti's home systems";

  inputs = rec {
    # marti shared settings
    shared.url = "gitlab:xvapx/shared?ref=development";
    # encrypt secrets
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "shared/nixpkgs";
    };
    # an anime game
    aagl = {
      url = "github:ezKEa/aagl-gtk-on-nix";
      inputs.nixpkgs.follows = "shared/nixpkgs";
    };
    # Nixos user repository
    nur.url = "github:nix-community/NUR";
  };

  outputs = { self, agenix, shared, aagl, nur }:
    let
      # nixpkgs lib + marti additions
      inherit (shared) lib;
      # flake git revision
      revision = "home-${self.shortRev or "DIRTY"}";
      # paths for nixPath
      paths = {
        nixpkgs = shared.nixpkgs.outPath;
        home-manager = shared.home-manager.outPath;
        dotfiles-marti = shared.dotfiles-marti.outPath;
      };
      # modules to import
      imports = {
        inherit agenix;
        inherit (shared) dotfiles-marti;
      };
      # organization settings
      organization = import ./organization.nix { inherit shared lib revision paths imports; };
      # nixpkgs with applied overlays
      nixpkgsFor = system:
        import shared.nixpkgs {
          inherit system;
          # allow unfree packages
          config.allowUnfree = true;
          overlays = [
            shared.overlays.default
            (final: prev: { 
            # add agenix binary
            agenix = agenix.packages.${system}.default;
            iir1 = prev.callPackage ./packages/iir1 {};
            })
          ];
        };

    in
    rec {

      # Used with `nixos-rebuild build --flake .#<hostname>`
      nixosConfigurations = {
        marti = import ./machines/marti.nix {
          inherit lib organization nixpkgsFor aagl nur;
        };
      };

      # Hydra jobs
      hydraJobs = {
        # Build the config.system.build.toplevel attribute of every machine in nixosConfigurations.
        machines = lib.mapAttrs ( name: value: value.config.system.build.toplevel ) nixosConfigurations;
      };

    };
}
