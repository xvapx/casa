{ config, lib, pkgs, revision, network, machine, dotfiles }:

{
  environment = {

    variables = {
      EDITOR = "vi";
      BROWSER = "firefox";
    };

    systemPackages = with pkgs; [
      keepassxc
      # cloud sync
      grive2
      # games
      crawlTiles
      ckan   # KSP Mod manager
      dosbox-staging
      gnome3.aisleriot
      gnome3.gnome-mahjongg
      gnome3.gnome-mines
      gnome3.gnome-sudoku
      rogue
      #libretro.pcsx2 2024-02-04 broken
      pcsx2
      (vbam.overrideAttrs ( final: prev: {
        buildInputs = [ glib gsettings-desktop-schemas ] ++ prev.buildInputs;
        nativeBuildInputs = [ wrapGAppsHook ] ++ prev.nativeBuildInputs;
      }))
      mangohud
      # downloads
      youtube-dl
      transgui
      amule-gui
      uget
      uget-integrator
      mangal
      qbittorrent
      # nix tools
      agenix
      # home
      gnucash
      # misc
      appimage-run
      wineWowPackages.waylandFull
      config.nur.repos.ataraxiasjel.waydroid-script
      mullvad-vpn
      looking-glass-client
    ];

  };

  programs = {
    anime-game-launcher.enable = true;
    honkers-launcher.enable = true;
    honkers-railway-launcher.enable = true;
  };

  system.stateVersion = "22.11";

  virtualisation = {
    docker = {
      enable = true;
      rootless = {
        enable = true;
        setSocketVariable = true;
      };
    };
    # Waydroid android emulator
    waydroid.enable = true;
    # needed for waydroid
    lxd.enable = true;
  };

  # Enable japanese input method
  i18n.inputMethod = {
    enabled = "fcitx5";
    fcitx5.addons = with pkgs; [
      fcitx5-mozc
      fcitx5-gtk
      libsForQt5.fcitx5-qt
      fcitx5-configtool
    ];
  };

  # Machine approximate location
  location = {
    latitude = 41.3945498;
    longitude = 2.1751121;
  };


  boot = {
    # delete all files in /tmp during boot
    tmp.cleanOnBoot = true;
    # latest Kernel
    kernelPackages = pkgs.linuxPackages_latest;
    # looking glass module
    extraModulePackages = [ pkgs.linuxPackages_latest.kvmfr ];
    extraModprobeConfig = ''
      options kvmfr static_size_mb=32
    '';
    kernelModules = [ "kvmfr" ];
  };

  services = {
    redshift = {
      enable = true;
      brightness.night = "0.8";
    };
    urxvtd.enable = true;
    mullvad-vpn.enable = true;
    printing = {
      enable = true;
      drivers = [ pkgs.hplip ];
    };
    udev.extraRules = ''
      SUBSYSTEM=="kvmfr", OWNER="xvapx", GROUP="kvm", MODE="0660"
    '';
  };

  # software roles
  marti.roles = {
    common = {
      enable = true;
      version_label = revision;
    };
    desktop = {
      enable = true;
      audio.normalized.enable = true;
      kde.enable = true;
      remote = {
        client = true;
      };
    };
    gaming = {
      enable = true;
      users = [ "xvapx" ];
      steam = {
        remotePlay = true;
        controller = true;
      };
      sony.dualshock4 = true;
      stadia.controller = true;
    };
    editor = {
      cad.enable = true;
      document = {
        enable = true;
        scan.users = [ "xvapx" ];
      };
      image.enable = true;
      multimedia.enable = true;
    };
    virtualization.libvirtd-kvm = {
      enable = true;
      users = [ "xvapx" ];
    };
    flatpak.enable = true;
    social.enable = true;
    developer = {
      enable = true;
      android = {
        enable = true;
        users = [ "xvapx" ];
      };
      database.enable = true;
      docker.enable = true;
      infosec.enable = true;
      nix.enable = true;
    };
    vpn = {
      enable = true;
      getflix.enable = true;
    };
  };

  # network mounts
  systemd = {
    mounts = [ {
      what = "//${network.casa.services.smb.magatzem.ip}/magatzem";
      where = "/mnt/magatzem";
      type = "cifs";
      options =
        "rw"
        + ",nofail"
        + ",vers=3.1.1"
        + ",credentials=${config.age.secrets.marti_smb_credentials.path}"
        + ",iocharset=utf8"
        + ",x-systemd.device-timeout=30s"
        + ",x-systemd.mount-timeout=30s";
    } {
      what = "//${network.casa.services.smb.downloads.ip}/downloads";
      where = "/mnt/downloads";
      type = "cifs";
      options =
        "rw"
        + ",nofail"
        + ",vers=3.1.1"
        + ",credentials=${config.age.secrets.marti_smb_credentials.path}"
        + ",iocharset=utf8"
        + ",x-systemd.device-timeout=30s"
        + ",x-systemd.mount-timeout=30s";
    } ];
    automounts = [ {
      where = "/mnt/magatzem";
      wantedBy = [ "multi-user.target" ];
    } {
      where = "/mnt/downloads";
      wantedBy = [ "multi-user.target" ];
    } ];
  };
}
