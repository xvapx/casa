{ lib, nixpkgsFor, organization, aagl, nur }:

let

  # machine network settings
  network = organization.network.casa.workstations.marti;
  # machine localization settings
  locale = organization.localization.machine.en_GB-es;
  # machine architecture
  system = "x86_64-linux";
  # pkgs collection based on machine architecture
  pkgs = nixpkgsFor system;
  # machine software
  software = { config, ... }: import ./marti-software.nix {
    inherit lib pkgs config;
    inherit (organization) revision network;
    machine = network;
    dotfiles = organization._dotfiles { inherit pkgs; };
  };
  # machine harware
  hardware = import ./marti-hardware.nix {
    inherit network;
  };

in

lib.nixosSystem {
  inherit system;
  modules = [
    software
    hardware
    locale
    aagl.nixosModules.default
    nur.nixosModules.nur
  ] ++ organization.modules;
}
