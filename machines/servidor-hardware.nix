{ network }:
let

  motherboard = {
    # sudo dmidecode | grep -A4 '^Base Board Information'
    vendor = "supermicro";
    model = "X8SIL";
    serial_number = "0123456789";
    # Either "UEFI" or "BIOS",
    # check for the existence of /sys/firmware/efi
    uefi = false;
    nvram = false;
    fans = {
      number = 4;
      original = "supermicro FAN-0065-L4";
      current = "aerocooler DF124028BU-PWMG";
    };
  };

  cpu = {
    # lscpu
    vendor = "intel";
    model = "Intel(R) XEON(R) CPU L3426 @ 1.87GHz";
    byte_order = "Little Endian";
    # use 'nproc --all' to determine this number, nixos default is 1.
    cores = 8;
  };

  gpu = {
    # lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1)
    vendor = "matrox";
    model = "MGA G200eW WPCM450 (rev 0a)";
    # Activate OpenGL
    opengl = false;
  };

  network_devices = {
    # sudo lshw -class network | grep -A 1 "bus info" | grep name | awk -F': ' '{print $2}'
    interfaces = {
      # "enp4s0" = network.interfaces.primary;   # bond slave
      # "enp5s0" = network.interfaces.secondary; # bond slave
      # "bond0" = network.interfaces.bond0;      # bridge slave
      "br0" = network.interfaces.bridge0;
    };
    # bond interfaces
    # see https://www.kernel.org/doc/Documentation/networking/bonding.txt
    bonds = {
      bond0 = {
        driverOptions = {
          mode = "balance-xor";
          xmit_hash_policy = "encap2+3";
          miimon = "100";
          downdelay = "200";
          updelay = "200";
        };
      interfaces = [ "enp4s0" "enp5s0" ];
      };
    };
    # bridge interfaces
    bridges = {
      br0 = {
        interfaces = [ "bond0" ];
      };
    };
  };

  disks = rec {

    system-1 = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sda";
      vendor = "western digital";
      model = "WD3000HLFS-01GU4";
      serial = "";
      # boot from this device?
      boot = true;
      # spin down this device when not in use?
      spin-down = true;
      # monitor this device with smartd?
      smartd = true;
      smartd-options = "-d sat";
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        boot = {
          filesystem = "raid";
        };
        nixos = {
          filesystem = "raid";
        };
        swap = {
          filesystem = "raid";
        };
      };
    };

    system-2 = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sdb";
      vendor = "western digital";
      model = "WD3000HLFS-01GU4";
      serial = "";
      # boot from this device?
      boot = true;
      # spin down this device when not in use?
      spin-down = true;
      # monitor this device with smartd?
      smartd = true;
      smartd-options = "-d sat";
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        boot = {
          filesystem = "raid";
        };
        nixos = {
          filesystem = "raid";
        };
        swap = {
          filesystem = "raid";
        };
      };
    };
    
    system-raid = {
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        boot = rec {
          # Partition uuid, get with 'blkid /dev/disk/by-label/boot'
          uuid = "0dcb9aac-8e77-4d18-be5f-6ccdb2ca2e6b";
          device = "/dev/disk/by-uuid/${uuid}";
          mountpoint = "/boot";
          filesystem = "ext4";
        };
        nixos = rec {
          # Partition uuid, get with 'blkid /dev/disk/by-label/nixos'
          uuid = "c3a5a55d-4365-4fc8-9d84-058a3e019a18";
          device = "/dev/disk/by-uuid/${uuid}";
          mountpoint = "/";
          filesystem = "ext4";
        };
        swap = rec {
          # Partition uuid, get with 'blkid /dev/md2'
          uuid = "4bf84f14-a574-4a51-8694-c21f5f22d47c";
          device = "/dev/disk/by-uuid/${uuid}";
          filesystem = "swap";
        };
      };
    };
    
    magatzem-1 = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sdc";
      vendor = "western digital";
      model = "WDC WD80EFAX-68KNBN0";
      serial = "VGJ0Z7SG";
      # spin down this device when not in use?
      spin-down = true;
      # monitor this device with smartd?
      smartd = true;
      smartd-options = "-d sat";
      # disable write cache for this device?
      disable_write_cache = true;
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        magatzem = {
          filesystem = "raid";
        };
      };
    };
    
    magatzem-2 = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sdd";
      vendor = "western digital";
      model = "WDC WD80EFAX-68KNBN0";
      serial = "VAH0RX6L";
      # spin down this device when not in use?
      spin-down = true;
      # monitor this device with smartd?
      smartd = true;
      smartd-options = "-d sat";
      # disable write cache for this device?
      disable_write_cache = true;
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        magatzem = {
          filesystem = "raid";
        };
      };
    };
    
    magatzem-raid = {
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        magatzem = rec {
          # Partition uuid, get with 'blkid /dev/md4'
          uuid = "e40be3d0-96b7-40f3-85ee-dc858b56be5d";
          device = "/dev/disk/by-uuid/${uuid}";
          mountpoint = "/mnt/magatzem";
          filesystem = "ext4";
          options = [ "nofail" ];
        };
      };
    };

  };

in rec {

  marti.profiles = {
    cpu = {
      inherit (cpu) vendor cores;
    };
    gpu = {
      inherit (gpu) vendor drivers xrandrHeads;
      openGl.enable = gpu.opengl;
    };
    network = {
      bluetooth.enable = network_devices.bluetooth or false;
      settings = { inherit (network) hostName hostId defaultGateway nameservers firewall; };
      inherit (network_devices) interfaces;
    };
    filesystems = {
      boot = {
        efi = {
          support = motherboard.uefi;
        };
        # boot raids even if degraded
        preLVMCommands = ''
          mdadm --run /dev/md1
          mdadm --run /dev/md2
          mdadm --run /dev/md3
          mdadm --run /dev/md4
        '';
      };
      kernel = {
        modules = [
          "ahci"
          "ohci_pci"
          "ehci_pci"
          "ata_piix"
          "pata_atiixp"
          "pata_jmicron"
          "firewire_ohci"
          "usbhid"
          "usb_storage"
          "sd_mod"
          "sr_mod"
          "kvm-intel"
          "dm-snapshot"
          "dm-raid"
          "br_netfilter"
          # sensors
          "coretemp"
          "jc42"
          "w83627ehf"
        ];
      };
      inherit disks;
    };
  };

}
