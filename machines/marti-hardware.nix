# marti machine hardware configuration

{ network }:
let

  motherboard = {
    # sudo dmidecode | grep -A4 '^Base Board Information'
    vendor = "Acer";
    model = "FMP55";
    serial_number = "U025094404148";
    # Either "UEFI" or "BIOS",
    # check for the existence of /sys/firmware/efi
    uefi = false;
  };

  cpu = {
    # lscpu
    vendor = "intel";
    model = "Intel(R) Core(TM) i5 CPU 750 @ 2.67GHz";
    byte_order = "Little Endian";
    # use 'nproc --all' to determine this number, nixos default is 1.
    cores = 4;
  };

  gpu = {
    # lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1)
    vendor = "amd";
    model = "Sapphire Technology Limited Nitro+ Radeon RX 580 4GB";
    # Activate OpenGL
    opengl = true;
    # Monitor configuration
    xrandrHeads = [
      { output = "HDMI-0"; }
      { output = "HDMI-1"; primary = true; }
    ];
    # Drivers
    drivers = [ "amdgpu" ];
  };

  network_devices = {
    # sudo lshw -class network | grep -A 1 "bus info" | grep name | awk -F': ' '{print $2}'
    interfaces = {
      "enp0s25" = network.interfaces.primary;
    };
    bluetooth = true;
  };

  disks = rec {

    system = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sdc";
      vendor = "western Digital";
      model = "WDC  WDS100T2B0A-00SM50";
      serial = "20269C804218";
      boot = true;
      smartd = true;
      smartd-options = "-d sat";
      # Partitions info, get with:
      # lsblk -o name,mountpoint,label,size,fstype,uuid
      partitions = {
        boot = rec {
          # Partition uuid, get with 'blkid /dev/sdb1'
          uuid = "7198b2cd-727a-4632-9b73-c05c46807dce";
          device = "/dev/disk/by-uuid/${uuid}";
          filesystem = "ext4";
          mountpoint = "/boot";
        };
        nixos = rec {
          # Partition uuid, get with 'blkid /dev/sdb1'
          uuid = "aac76609-857c-4692-90a8-0b9fcbf7e5ef";
          device = "/dev/disk/by-uuid/${uuid}";
          filesystem = "xfs";
          mountpoint = "/";
        };
      };
    };

    windows = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sda";
      vendor = "seagate";
      model = "ST1000DM003-1SB102";
      serial = "W9A42DNZ";
      smartd = true;
      smartd-options = "-d sat";
      spin-down = true;
      disable_write_cache = true;
      # Partitions info, get with:
      # lsblk -o name,mountpoint,label,size,fstype,uuid
      partitions = {
        windows-system-reserved = {
          device = "/dev/sda1";
          filesystem = "ntfs";
        };
        windows = {
          device = "/dev/sda2";
          filesystem = "ntfs";
          mountpoint = "/mnt/windows";
          options = [ "noauto" ];
        };
      };
    };

    data = {
      # Device info, get with:
      # sudo hdparm -I /dev/sdc
      device = "/dev/sdb";
      vendor = "seagate";
      model = "ST2000DM001-1ER164";
      serial = "S4Z0BWNT";
      smartd = true;
      smartd-options = "-d sat";
      spin-down = true;
      disable_write_cache = true;
      # Partitions info, get with:
      # lsblk -o name,mountpoint,label,size,fstype,uuid
      partitions = {
        swap = {
          label = "swap";
          device = "/dev/disk/by-label/swap";
          filesystem = "swap";
        };
        data = {
          label = "data";
          filesystem = "xfs";
          mountpoint = "/mnt/data";
          options = [ "nofail" ];
        };
      };
    };

  };

in rec {

  marti.profiles = {
    cpu = {
      inherit (cpu) vendor cores;
    };
    gpu = {
      inherit (gpu) vendor drivers xrandrHeads;
      openGl.enable = gpu.opengl;
    };
    network = {
      bluetooth.enable = network_devices.bluetooth or false;
      settings = { inherit (network) hostName hostId defaultGateway nameservers firewall; };
      inherit (network_devices) interfaces;
    };
    filesystems = {
      boot = {
        efi = {
          support = motherboard.uefi;
        };
      };
      kernel = {
        sysctl = {
          # low swappiness
          "vm.swappiness" = 10;
        };
        modules = [
          "ahci"
          "ohci_pci"
          "ehci_pci"
          "ata_piix"
          "pata_atiixp"
          "pata_jmicron"
          "firewire_ohci"
          "usbhid"
          "usb_storage"
          "sd_mod"
          "sr_mod"
          "amdgpu"
        ];
      };
      inherit disks;
    };
    yubikey.enable = true;
  };

}
