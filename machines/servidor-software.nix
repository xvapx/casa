{ config, lib, pkgs, revision, network, machine, dotfiles }:

{
  environment = {
    variables = {
      EDITOR = "vi";
    };
    systemPackages = with pkgs; [ ];
  };

  # Machine approximate location
  location = {
    latitude = 41.3945498;
    longitude = 2.1751121;
  };
  
  system.stateVersion = "22.05";

  # delete all files in /tmp during boot
  boot.tmp.cleanOnBoot = true;

  services = {
    
    # Enable samba sharing
    samba = {
      enable = true;
      extraConfig = ''
        [global]
        workgroup = SPIDER
        server string = ${machine.hostName}
        netbios name = ${machine.hostName}
        server role = standalone server
        hosts allow = 192.168.0.0/24 localhost
        hosts deny = 0.0.0.0/0
        security = user
        invalid users = root marti_admin
        encrypt passwords = yes
        log level = 3
        max log size = 1000
        unix password sync = yes
        printcap name = /dev/null
        force group = +smb_users
      '';
      shares = {
        magatzem = {
          comment = "Magatzem";
          path = "/mnt/magatzem/public";
          browseable = "yes";
          "read only" = "yes";
          "guest ok" = "no";
          "valid users" = "invitado @smb_users";
          "read list" = "invitado";
          "write list" = "@smb_users";
          "create mask" = "0775";
          "directory mask" = "0775";
        };
        downloads = {
          comment = "Downloads";
          path = "/mnt/magatzem/downloads";
          browseable = "yes";
          "read only" = "yes";
          "guest ok" = "no";
          "valid users" = "invitado @smb_users";
          "read list" = "invitado";
          "write list" = "@smb_users";
          "create mask" = "0775";
          "directory mask" = "0775";
        };
      };
    };

    # Enable minidlna server
    minidlna = {
      enable = true;
      mediaDirs = [ "V,/mnt/magatzem/public/Video" "A,/mnt/magatzem/public/Audio" ];
      loglevel = "general,artwork,database,inotify,scanner,metadata,http,ssdp,tivo=debug";
      announceInterval = 5;
      extraConfig = ''
        # monitor media directories in real time
        inotify=yes
        # SSDP notify interval, in seconds.
        notify_interval=1
        force_sort_criteria=+upnp:class,+upnp:originalTrackNumber,+dc:title
      '';
    };
      
    # ClamAV
    clamav = {
      daemon = {
        enable = true;
        settings = {
          LogSyslog = true;
        };
      };
      updater = {
        enable = true;
      };
    };

    # Guests Wireless Access Point
    hostapd = {
      # TODO: enable guest wifi
      enable = false;
      #interface = "${hardware.network.interfaces.external}";
      ssid = "invitados";
      channel = 3;
      wpa = true;
      wpaPassphrase = "";
    };

    # Transmission bittorrent
    transmission = {
      enable = true;
      group = "smb_users";
      settings = {
        download-dir = "/mnt/magatzem/downloads/transmission/complete";
        incomplete-dir = "/mnt/magatzem/downloads/transmission/incomplete";
        incomplete-dir-enabled = true;
        rpc-bind-address = "0.0.0.0";
        rpc-whitelist = "127.0.0.1,192.168.0.*";
        download-queue-enabled = false;
        ratio-limit = 5.0;
        ratio-limit-enabled = true;
      };
    };

    # Amule daemon
    # run `amuled --ec-config` to configure the daemon
    amule = {
      # TODO: enable amule daemon
      enable = false;
      # Make sure to chgrp this directory
      dataDir = "/mnt/magatzem/downloads/amule";
      #package = channels.local-spider-master.amuleDaemon;
    };

  };

  # software roles
  marti.roles = {
    common = {
      enable = true;
      version_label = revision;
    };
    desktop = {
      enable = false;
    };
    server = {
      amule = {
        enable = true;
        openfirewall = false;
      };
    };
    virtualization.libvirtd-kvm = {
      enable = true;
      users = [ "xvapx" ];
    };
  };

}
