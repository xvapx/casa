{ shared }:

rec {

  casa = {

    routers = {
      external = {
        interfaces.primary = {
          vendor = "SerComm";
          ip = "192.168.0.1";
        };
      };
    };

    servers = {
      supermicro = {
        inherit (defaults) defaultGateway nameservers;
        interfaces = {
          primary = {
            # ip = ""; # bond slaves do not have IP
            mac = "00:11:D8:2B:5C:76";
          };
          secondary = {
            # ip = ""; # bond slaves do not have IP
            mac = "00:11:D8:2B:5C:76";
          };
          bond0 = {
            ip = "192.168.0.10";
            inherit (defaults) prefixLength;
          };
          wireless = {
            ip = "192.168.1.1";
          };
        };
        hostName = "supermicro";
        # generated with 'head -c4 /dev/urandom | od -A none -t x4'
        hostId = "030ab98b";
        firewall = defaults.firewall // {
          allowedTCPPorts = [
            22     # openssh
            445    # Samba
            139    # Samba Netbios
            1110   # xrdp server
            4712   # aMuled ECServer
            4411   # aMuled
            5001   # iperf server
            8200   # miniDLNA
            9091   # transmission RPC interface
            16553  # transmission
          ];
          allowedUDPPorts = [
            137    # Samba Netbios Name Service
            138    # Samba Netbios Datagram Service
            139    # Samba Netbios Session Service
            4751   # aMuled client socket
            16553  # transmission
          ];
        };
      };
    };

    workstations = {
      marti = {
        inherit (defaults) defaultGateway nameservers;
        interfaces.primary = {
          ip = "192.168.0.40";
          mac = "00:01:6c:70:d7:e0";
          inherit (defaults) prefixLength;
        };
        hostName = "marti";
        # generated with 'head -c4 /dev/urandom | od -A none -t x4'
        hostId = "898f0cc8";
        firewall = defaults.firewall // {
          allowedTCPPorts = [
            7759   # tribler
            7760   # tribler
            50500  # Freely avaliable open port
          ];
          allowedUDPPorts = [
            7759   # tribler
            7760   # tribler
            6771   # tribler
            50500  # Freely avaliable open port
          ];
        };
      };
    };

    services = rec {
      supermicro.ip = casa.servers.supermicro.interfaces.bond0.ip;
      smb.magatzem.ip = supermicro.ip;
      smb.downloads.ip = supermicro.ip;
    };

  };

  # default setting for this organization's machines
  defaults = {
    prefixLength = 24;
    defaultGateway = casa.routers.external.interfaces.primary.ip;
    nameservers = shared.nameservers.cloudflare.ipv4;
    firewall = {
      enable = true;
      allowPing = true;
    };
    inherit (shared) nixosBinaryCaches binaryCachePublicKeys;
    sshPorts = [ 22 ];
  };

}
