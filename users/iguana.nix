# Iguana user settings

{ user_defaults, dotfiles }:

let

  name = "iguana";
  fullName = "Iguana";
  uid = 1031;
  home = "/home/${name}";
  locale = {
    lang = "es_ES.UTF-8";
    languages = "es:ca:en";
  };
  default = user_defaults { inherit name uid home; };

in
{

  # user-specific activations scripts
  system.activationScripts."user-${name}" = default.activation.directories;

  # user specific system settings
  users.users."${name}" = default.general // {
    description = fullName;
    extraGroups = [ "audio" "cdrom" "networkmanager" "scanner" "video" "wheel" "xrdp" ];
    openssh.authorizedKeys.keys = [ ];
  };

  # home-manager user settings
  home-manager.users."${name}" = default.home-manager;

}
