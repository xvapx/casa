{ lib, pkgs, user_defaults, dotfiles, localization }:

let

  name = "xvapx";
  fullName = "Marti Serra";
  uid = 1030;
  home = "/home/${name}";
  email = "marti.serra@protonmail.com";
  default = user_defaults { inherit name uid home; };

  secret = file: {
    owner = name;
    inherit file;
  };

in
{

  # user-specific activations scripts
  system.activationScripts."user-${name}" = default.activation.directories;

  # user specific system settings
  users.users."${name}" = default.general // {
    description = fullName;
    extraGroups = [ "audio" "cdrom" "networkmanager" "scanner" "video" "wheel" "xrdp" "docker" ];
    openssh.authorizedKeys.keys = [ ];
    packages = with pkgs; [
      helix
    ];
  };

  # User agenix-managed secrets
  age.secrets = {
    marti_gdrive_id = secret ../secrets/marti_gdrive_id.age;
    marti_gdrive_key = secret ../secrets/marti_gdrive_key.age;
    marti_smb_credentials = {
      file = ../secrets/marti_smb_credentials.age;
    # for systemd automount, this file must be explicitly readable by root
      owner = "root";
      group = "root";
    };
    marti_getflix_ca = secret ../secrets/marti_getflix_ca.age;
    marti_getflix_auth = secret ../secrets/marti_getflix_auth.age;
  };

  # home-manager user settings
  home-manager.users."${name}" = lib.recursive_merge [
    default.home-manager
    { home.file = {
        ".bash_locale".text = localization.user.bash.en;
        ".config/plasma-localerc".text = localization.user.plasma.en;
        # TODO: auto-generate trevol values in trevol repo and get them from there
        ".ssh/config".text = dotfiles.ssh.config {
          inherit home;
          auto-add = true;
          pre = ''
Host github gitlab
  HostName %h.com
  User git
  IdentityFile ${home}/.ssh/git

##### Trevol ##################################################################

# port is always 22 for *-local hosts
Host trevol/*-local
  port 22

Host trevol/administracio*
  port 2052
  User vicenç
Host trevol/administracio-local
  HostName 192.168.143.052

Host trevol/comercial*
  port 2050
  User comercial
Host trevol/comercial-local
  HostName 192.168.143.050

Host trevol/comptabilitat*
  port 2051
  User comptabilitat
Host trevol/comptabilitat-local
  HostName 192.168.143.051

Host trevol/infres*
  port 2053
  User infres
Host trevol/infres-local
  HostName 192.168.143.053

Host trevol/neteja-1*
  port 2054
  User neteja
Host trevol/neteja-1-local
  HostName 192.168.143.054

Host trevol/neteja-2*
  port 2055
  User neteja
Host trevol/neteja-2-local
  HostName 192.168.143.055

Host trevol/informatica*
  port 2059
  User marti
Host trevol/informatica-local
  HostName 192.168.143.059

Host trevol/tipsa*
  port 2061
  User enric
Host trevol/tipsa-local
  HostName 192.168.143.061

Host trevol/trafic-1*
  port 2060
  User manolo
Host trevol/trafic-1-local
  HostName 192.168.143.060

Host trevol/trafic-2*
  port 2062
  User montse
Host trevol/trafic-2-local
  HostName 192.168.143.062

Host trevol/servidor*
  port 2030
  User administrator
Host trevol/servidor-local
  HostName 192.168.143.030

Host trevol/linux*
  port 2045
  User marti
Host trevol/linux-local
  HostName 192.168.143.045

Host trevol/*
    IdentityFile ${home}/.ssh/trevol
    # Will apply only to Hosts without HostName (remote)
    HostName 2.136.119.107

###############################################################################
##### default configuration ###################################################

Host *
    ForwardAgent no
    ForwardX11 no
    ForwardX11Trusted no
    Port 22
    Protocol 2
    ServerAliveInterval 60
    ServerAliveCountMax 5
    TCPKeepAlive no
    AddKeysToAgent yes

###############################################################################
        '';
        };
    };}
  ];

  marti.dotfiles = {
    enable = true;
    user = { inherit name email home uid; full_name = fullName; };
    git = {
      enable = true;
      editor = "nvim";
    };
    neovim = {
      enable = true;
      default_editor = true;
    };
    tmux.enable = true;
    bash = {
      enable = true;
      ssh-agent.start = true;
      virtualization.aliases = true;
      tmux.start = true;
    };
    gdrive = {
      enable = true;
      hosts = [ "marti" ];
    };
    steam = {
      enable = true;
      autostart = true;
    };
  };

}
